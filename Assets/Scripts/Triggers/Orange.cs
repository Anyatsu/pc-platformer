﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class Orange : Collectible
{
    private void Awake()
    {
        SetUp();
    }

    private void Start()
    {
        CheckDatabase();
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            Collect();
        } 
    }
}