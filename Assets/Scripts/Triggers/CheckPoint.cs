﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    [SerializeField] Vector2Variable spawnPosition;
    [SerializeField] GameEvent triggerEvent;
    [SerializeField] GameEvent newCheckpoint;
    [SerializeField] GameEventListener deactivateListener;
    private bool active;
    private Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        //if (spawnPosition.value == (Vector2)transform.position)
        //{
        //    active = true;
        //    deactivateListener.enabled = true;
        //}
        //else
        //    active = false;
        //animator.SetBool("Active", active);
    }

    public void Deactivate()
    {
        active = false;
        animator.SetBool("Active", active);
        deactivateListener.enabled = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            triggerEvent.Raise();

            if (!active)
            {
                spawnPosition.value = transform.position;
                newCheckpoint.Raise();
                active = true;
                animator.SetBool("Active", active);
                deactivateListener.enabled = true;              
            }

        }
    }
}
