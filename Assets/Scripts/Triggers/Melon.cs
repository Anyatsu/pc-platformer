﻿using System.Collections;
using System.Collections.Generic;
//using System.Runtime.Remoting.Messaging;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Melon : Collectible
{
    private GameEventListener checkpointEventListener;
    [SerializeField] Transform parentOnCollision;
    [SerializeField] float offset;

    [SerializeField] private float returnDuration = 1;
    private Vector2 startPosition;
    Quaternion initRotation;

    private void Awake()
    {
        SetUp();
        checkpointEventListener = GetComponent<GameEventListener>();
        checkpointEventListener.enabled = false;
        parentOnCollision = FindObjectOfType<PlayerController>().transform;
        startPosition = transform.position;
        initRotation = transform.rotation;
    }

    private void Start()
    {
        CheckDatabase();

    }





    public void ReturnToStart()
    {
        StopAllCoroutines();
        StartCoroutine(LerpPosition(startPosition));
    }

    IEnumerator LerpPosition(Vector2 position)
    {
        float t = 0;
        Vector2 startPosition = transform.position;
        while (t <= 1)
        {
            t += Time.deltaTime / returnDuration;
            transform.position = Vector2.Lerp(startPosition, position, t);
            yield return null;
        }
    }

    IEnumerator FollowTarget()
    {
        float t = 0;
        Vector2 startPosition = transform.position;
        while (t <= 1)
        {
            t += Time.deltaTime / returnDuration;
            transform.position = Vector2.Lerp(transform.position, parentOnCollision.position + Vector3.up * offset, t);
            yield return null;
        }

        while (true)
        {
            transform.position = parentOnCollision.position + Vector3.up * offset;
            yield return null;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            StartCoroutine(FollowTarget());
            checkpointEventListener.enabled = true;

        }
    }

}
