﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerScript : MonoBehaviour
{
    private MovingFloor scrpt;
    private Rigidbody2D RB;
    private SpriteRenderer wallSprite;
    private BoxCollider2D wallCollider;
    public GameObject wallObject;
    private void Awake()
    {
        scrpt = GetComponentInParent<MovingFloor>();
        RB = GetComponentInParent<Rigidbody2D>();
        wallSprite = wallObject.GetComponent<SpriteRenderer>();
        wallCollider = wallObject.GetComponent<BoxCollider2D>();



    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            scrpt.enabled = true;
            RB.gravityScale = 1;
            print(2);
            wallSprite.enabled = true;
            wallCollider.enabled = true;
           
        }
       
    }
   

}
