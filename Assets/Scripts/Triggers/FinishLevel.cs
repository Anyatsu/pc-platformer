﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLevel : MonoBehaviour
{

    private LevelComplete levelComplete;

    private void Awake()
    {
        levelComplete = FindObjectOfType<LevelComplete>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            levelComplete.ShowFinishScreen();
        }
    }
}
