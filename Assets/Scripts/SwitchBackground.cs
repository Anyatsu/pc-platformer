﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchBackground : MonoBehaviour
{
    [SerializeField] GameObject upperBackground;
    [SerializeField] GameObject lowerBackground;

    Collider2D trigger;

    private void Awake()
    {
        trigger = GetComponent<Collider2D>();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (collision.transform.position.y > trigger.bounds.center.y)
            {
                upperBackground.SetActive(true);
                lowerBackground.SetActive(false);

            }
            else
            {
                upperBackground.SetActive(false);
                lowerBackground.SetActive(true);
            }
        }
    }

}
