﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Tilemaps;

public class DestroyCeiling : MonoBehaviour
{
    private BoxCollider2D coll;
    private SpriteRenderer spriterender;
    public Tilemap ground;
    void Start()
    {
        coll = this.GetComponent<BoxCollider2D>();
        spriterender = this.GetComponent<SpriteRenderer>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            ground.SetTile(new Vector3Int(0, 0, 0), null);
            ground.SetTile(new Vector3Int(1, 0, 0), null);
        }

    }
}
