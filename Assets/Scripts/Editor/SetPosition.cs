﻿using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
public static class EditorHotkeysTracker
{
    static EditorHotkeysTracker()
    {
        SceneView.duringSceneGui += view =>
        {
            Event e = Event.current;
            if (e != null && e.control && e.keyCode == KeyCode.M)
            {
                Vector3 mousePos = e.mousePosition;
                float ppp = EditorGUIUtility.pixelsPerPoint;
                mousePos.y = view.camera.pixelHeight - mousePos.y * ppp;
                mousePos.x *= ppp;

                Vector2 inWorld = view.camera.ScreenToWorldPoint(mousePos);
                Transform target = GameObject.Find("StartPosition").transform;
                Undo.RegisterCompleteObjectUndo(target, "Set selected position");
                target.position = inWorld;
                
            }
        };
    }
}