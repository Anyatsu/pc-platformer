﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingFloor : MonoBehaviour
{
    public float maxDistanceFromStart;
    private Vector3 startingPosition;
    [SerializeField] private float acceleration;

    private Rigidbody2D rb;
    public float CurrentVelocity
    {
        get { return rb.velocity.y; }
    }

    private void Awake()
    {
        startingPosition = transform.position;
        rb = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        if (Mathf.Abs(rb.velocity.x) > Mathf.Sqrt(Mathf.Abs(acceleration) * maxDistanceFromStart))
        {
            acceleration = -acceleration;
        }
        rb.velocity += (Vector2)transform.up * acceleration * Time.deltaTime;

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        if (!Application.isPlaying)
            startingPosition = transform.position;
        Gizmos.DrawLine(startingPosition, startingPosition + (transform.right * maxDistanceFromStart));
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
           

        }
    }
}
