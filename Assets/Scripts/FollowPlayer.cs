﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour
{
    
    public Transform target; //set target from inspector instead of looking in Update
    public float speed = 3f;


    private void Awake()
    {
        target = FindObjectOfType<PlayerController>().transform;

        transform.SetParent(target);
        Invoke("Unparent", Time.deltaTime);
    }

    private void Unparent()
    {
        transform.SetParent(null);
    }

    void Update()
    {

        //rotate to look at the player
        transform.LookAt(target.position);
        transform.Rotate(new Vector3(0, -90, 0), Space.Self);//correcting the original rotation


        //move towards the player
        if (Vector3.Distance(transform.position, target.position) > 2f)
        {//move if distance from target is greater than 1
            transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));
        }
    


    }

}