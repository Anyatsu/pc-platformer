﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControll : MonoBehaviour
{
    private Transform player;
    [SerializeField] Vector2Variable playerVelocity;
    Transform currentTarget;

    private Vector2 targetPos;
    [SerializeField] Vector2Variable spawnPosition;
    public float smoothTime = 0.3F;
    private Vector2 velocity = Vector2.zero;

    [SerializeField] private Vector2Variable offset;

    private bool stopYMovement;

    Camera cam;
    [SerializeField] LayerMask bumperLayer;
    [SerializeField] float distanceOnBumper = 5;



    private void Awake()
    {
        offset.value = Vector2.zero;
        cam = GetComponent<Camera>();
    }

    private void Start()
    {
        transform.position = spawnPosition.value;
        player = FindObjectOfType<PlayerController>().transform;
        currentTarget = player;

    }

    void LateUpdate()
    {
        if (currentTarget != player)
        {
            offset.value = Vector2.zero;
        }
        Vector2 newPosition = Vector2.SmoothDamp(transform.position, targetPos + offset.value, ref velocity, smoothTime); ;
        transform.position = newPosition;

        //if (stopYMovement)
        //{
        //    transform.position = new Vector2(newPosition.x, transform.position.y);
        //}
        //else
        //{
        //    transform.position = newPosition;
        //}
    }

    public void StopYMovement(bool value)
    {
        stopYMovement = value;
    }

    private void FixedUpdate()
    {

        RaycastHit2D hit = Physics2D.Raycast(player.position, Vector2.down, 5, bumperLayer);
        if (hit)
        {
            targetPos = new Vector2(player.position.x, hit.collider.bounds.max.y + distanceOnBumper);
            currentTarget = hit.transform;
        }
        else 
        {
            //Making sure to set target as player only if he isn't falling to his death
            if (playerVelocity.value.y >= 0)
            {
                currentTarget = player;
                targetPos = player.position;
            }
            else if (playerVelocity.value.y < 0)
            {
                if (currentTarget == player)
                {
                    targetPos = player.position;
                }
            }
               
        }
    }
}
