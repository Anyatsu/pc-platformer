﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadZoneDetector : MonoBehaviour
{
    CameraControll cam;
    private void Awake()
    {
        cam = FindObjectOfType<CameraControll>();
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("DeadZone"))
        {
            print(1);
            cam.StopYMovement(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("DeadZone"))
        {
            cam.StopYMovement(false);
        }
    }
}
