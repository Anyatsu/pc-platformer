﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    public Vector2 length;
    private Vector2 startpos;
    private Transform cam;
    [SerializeField] private Vector2 parallaxEffect;
    private Vector2 cameraLocation;
    // Start is called before the first frame update
    private void Awake()
    {
        startpos = transform.position;
        cam = FindObjectOfType<CameraControll>().transform;
        length = GetComponent<SpriteRenderer>().bounds.size;

    }

    // Update is called once per frame
    void Update()
    {

        cameraLocation = cam.position * (Vector2.one - parallaxEffect);
        Vector2 travelDist = cam.position * parallaxEffect;

        transform.position = startpos + travelDist;
        
        startpos += Vector2.right * CheckDistance(cameraLocation.x, startpos.x, length.x);
        startpos += Vector2.up * CheckDistance(cameraLocation.y, startpos.y, length.y);

    }

    float CheckDistance(float location, float position, float length)
    {
        float moveDistance = 0;
        if (location > position + length)
        {
            moveDistance = length;
        }
        else if (location < position - length)
        {
            moveDistance = -length;
        }

        return moveDistance;
    }

    private void OnDrawGizmos()
    {
        if (!Application.isPlaying)
        {
            length = GetComponent<SpriteRenderer>().bounds.size;
        }
    }
}
