﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetUpLevel : MonoBehaviour
{
    [SerializeField] private GameEvent levelStartEvent;
    [SerializeField] private IntVariable[] valuesToReset;
    [SerializeField] private Vector2Variable spawnPosition;
    [SerializeField] private Transform startingPositionHolder;

    private static bool setupFinished;


    private void Awake()
    {
        if (!setupFinished)
        {
            foreach(IntVariable intVariable in valuesToReset)
            {
                intVariable.value = 0;
            }
            spawnPosition.value = startingPositionHolder.position;
            

        }

    }

    private void Start()
    {
        if (!setupFinished)
        {
            levelStartEvent.Raise();
            setupFinished = true;

        }
    }

    public void ResetSetup()
    {
        setupFinished = false;
    }
}
