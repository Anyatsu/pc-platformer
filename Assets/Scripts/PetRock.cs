﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Assertions.Must;
using UnityEngine.UIElements;

public class PetRock : MonoBehaviour
{
    public float speed;
    private Transform target;
    private Rigidbody2D body;
    private Vector2[] position;
    private int i;
    private int j;
    private bool startMoving;
    public int size = 100;
    private bool facingRight;
    public float distance;
    private Animator animator;
    private enum Direction
    {
        Left = -1,
        Right = 1
    };
    private Direction currentDirection;
    private Rigidbody2D rb;
    private void Awake()
    {
        body = GetComponent<Rigidbody2D>();
        position = new Vector2[size];
        i = 0;
        j = 0;
        startMoving = false;
        
    }
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        animator = GetComponent<Animator>();
        rb = transform.GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        j = i-10;
        if (i < 10)
        {
            j = 90 + i;
        }
        
        position[i] = (Vector2)target.position;
        i++;
        StartCoroutine(waiter());
        if (startMoving && Vector2.Distance(transform.position, target.position) > distance)
        {
            transform.position = position[j];
            j++;
        }
        if (i >= size)
            i = 0;
        if (j >= size)
            j = 0;
        animator.SetFloat("Velocity", rb.velocity.magnitude);
    }
    IEnumerator waiter()
    {
        yield return new WaitForSeconds(0.25f);
        startMoving = true;
    }
   
}
