﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    [SerializeField] protected IntVariable collectible;
    [SerializeField] protected IntVariable amountInLevel;
    [SerializeField] protected GameEvent pickupEvent;
    [SerializeField] protected GameObject pickupEffect;

    protected float id;
    protected static Dictionary<float, bool> collectedDatabase;

    protected void SetUp()
    {
        id = transform.position.sqrMagnitude;

        if (collectedDatabase == null)
        {
            collectedDatabase = new Dictionary<float, bool>();
        }
    }

    protected void CheckDatabase()
    {
        if (collectedDatabase.ContainsKey(id))
        {
            if (collectedDatabase[id])
                Destroy(gameObject); //we've been collected already; destroy ourself
        }
        else
        {
            collectedDatabase.Add(id, false);
        }
    }

    public void Collect()
    {
        collectible.value++;
        collectedDatabase[id] = true;
        pickupEvent.Raise();
        Instantiate(pickupEffect, transform.position, Quaternion.identity);

        Destroy(gameObject);
    }

    public void IncreaseAmountInLevel()
    {
        amountInLevel.value++;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {

        }
    }
}
