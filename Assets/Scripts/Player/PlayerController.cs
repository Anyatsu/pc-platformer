﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController : MonoBehaviour
{
    [SerializeField] ParticleSystem dust;
    [SerializeField] Vector2Variable spawnPosition;
    [SerializeField] Vector2Variable cameraOffset;

    [Header("Components")]
    private SpriteRenderer sprite;
    private Animator animator;
    private GameObject characterHolder;
    private Rigidbody2D rb;
    private Collider2D playerCollider;
    private AudioSource audioSource;

    [Header("Movement")]
    public float accelerationRate = 20f;
    public float slowdownSpeed = 4f;
    public float maxSpeed = 7f;
    private Vector2 dir;
    public bool FacingRight { get; private set; } = true;
    private bool canMove;

    [SerializeField] Vector2Variable velocityHolder;

    private bool onMovingPlatform;
    private MovingPlatform currentPlatform;

    [Header("Grounding")]
    public bool isGrounded = true;
    public LayerMask groundMask;
    public float groundSkin;
    public float jumpBoxWidth = 2f;
    public float jumpBoxHeight;
    public float delayBeforeUngrounding = 0.1f;
    private float ungroundTimer;


    [Header("Jumping")]
    public float jumpMultiplier = 5f;
    public float fallMultiplier = 2.5f;
    public float lowJumpMultiplier = 2f;
    public float jumpDelay = 0.25f;
    private float jumpTimer;
    [SerializeField] private float holdFallMultiplier;

    [SerializeField] private float maxFallSpeed;
    [SerializeField] private float holdMaxFallSpeed = -25f;
    [SerializeField] private float currentMaxFallSpeed;

    [SerializeField] private float midAirTurnSpeed = 2.5f;
    [SerializeField] private float normalSpeedThreshold = 3f;

    bool JumpInQueue { get { return Time.time < jumpTimer && Time.time < ungroundTimer; } }
    

    [Header("Jump Squeeze")]
    public float jumpXSqueeze;
    public float jumpYSqueeze;
    public float jumpSqueezeDuration;
    public float landXSqueeze;
    public float landYSqueeze;
    public float landSqueezeDuration = 0.1f;

    [Header("Wall Jump")]
    [SerializeField] LayerMask wallLayer;
    [SerializeField] Vector2 wallBoxSize;
    [SerializeField] private float wallSlideSpeed;
    [SerializeField] private float wallJumpHeight;
    [SerializeField] private float slowdownOnWallCollision;
    [SerializeField] private float maxWallSlideSpeed = 7;
    [SerializeField] private float wallJumpDelay;
    [SerializeField] private float regainControlDelay = 0.1f;
    private bool onWall;
    private bool onRightWall;
    private float wallJumpTimer;
    private Vector2 lastPosOnWall;

    [Header("Sounds")]
    [SerializeField] private AudioClip deathClip;
    [SerializeField] private AudioClip landClip;
    [SerializeField] private AudioClip jumpClip;
    [SerializeField] private AudioClip enemyStomp;

    [Header("Camera Offset")]
    [SerializeField] private float offsetLerpDuration = 0.5f;
    [SerializeField] private Vector2 maxFallCameraOffset = new Vector2(0, -5);
    [SerializeField] private float cameraRunOffset = 1;
    enum CameraPosition { Fall, Normal, Right, Left}
    private CameraPosition cameraPosition;
    private Coroutine activeLerp;


    [Header("Death")]
    [SerializeField] private float deathMoveMultiplier = 5;
    [SerializeField] private GameEvent playerDeathEvent;
    Coroutine playerDeathCo;


    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        characterHolder = transform.GetChild(0).gameObject;
        sprite = characterHolder.GetComponent<SpriteRenderer>();
        animator = characterHolder.GetComponent<Animator>();
        playerCollider = GetComponent<Collider2D>();
        audioSource = GetComponent<AudioSource>();

        canMove = true;
        
    }

    private void Start()
    {
        transform.position = spawnPosition.value;

    }
    // Update is called once per frame
    void Update()
    {
        if (canMove)
        {
            dir = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        }
        else
        {
            dir = Vector2.zero;
            return;
        }

        if (!onWall)
        {
            if ((dir.x > 0 && !FacingRight) || (dir.x < 0 && FacingRight))
            {
                Flip();

            }
        }
        else
        {
            if (FacingRight != onRightWall)
            {
                Flip();
                
            }
        }


        if (Input.GetButtonDown("Jump"))
        {
            jumpTimer = Time.time + jumpDelay;
        }
    }

    void FixedUpdate()
    {
        MoveCharacter();

        //Checking if player either pressed jump a few momment before landing or
        //pressed jump a few moments after leaving the ground
        if (JumpInQueue)
        {
            Jump(jumpMultiplier);
        }
        else if (Time.time < jumpTimer && Time.time < wallJumpTimer)
        {
            WallJump();
        }

        CheckCollision();
        ModifyPhysics();

        velocityHolder.value = rb.velocity;

    }

    void MoveCharacter()
    {
        if (onMovingPlatform && dir.x == 0)
        {
            rb.velocity = new Vector2(currentPlatform.CurrentVelocity, rb.velocity.y);

            animator.SetFloat("Speed", 0);
        }
        else
        {
            float acceleration = accelerationRate;
            if (!isGrounded && Mathf.Abs(rb.velocity.x) < normalSpeedThreshold)
            {
                acceleration *= midAirTurnSpeed;
            }

            rb.AddForce(Vector2.right * dir.x * acceleration);
            if (Mathf.Abs(rb.velocity.x) > maxSpeed)
            {
                rb.velocity = new Vector2(Mathf.Sign(rb.velocity.x) * maxSpeed, rb.velocity.y);
                LerpCameraOffset(FacingRight ? CameraPosition.Right : CameraPosition.Left);
            }

            animator.SetFloat("Speed", Mathf.Abs(rb.velocity.x));


        }

        if (dir.y < 0)
        {
            rb.AddForce(Vector2.up * dir.y * holdFallMultiplier);
            currentMaxFallSpeed = holdMaxFallSpeed;
        }
        else
        {
            currentMaxFallSpeed = maxFallSpeed;
        }
    }

 

    IEnumerator DisableMovement(float time)
    {
        canMove = false;
        yield return new WaitForSeconds(time);
        canMove = true;

    }

    void CheckCollision()
    {
        animator.SetBool("onWall", onWall);
        CheckGroundCollision();
        if (!isGrounded)
        {
            CheckWallCollision();
        }



    }

    void CheckGroundCollision()
    {
        Vector2 boxCenter = (Vector2)transform.position + Vector2.down * jumpBoxHeight;
        bool wasGrounded = isGrounded;
        Collider2D underPlayer = Physics2D.OverlapBox(boxCenter, new Vector2(jumpBoxWidth, groundSkin), 0, groundMask);

        if (underPlayer == null)
        {
            isGrounded = false;
        }
        else
        {
            isGrounded = true;
            onWall = false;
            ungroundTimer = Time.time + delayBeforeUngrounding;
            if (!wasGrounded)
            {
                StartCoroutine(JumpSqueeze(landXSqueeze, landYSqueeze, landSqueezeDuration));
                LerpCameraOffset(CameraPosition.Normal);

                PlayAudioClip(landClip);
                Instantiate(dust, new Vector2(transform.position.x, playerCollider.bounds.min.y), Quaternion.identity);
            }

        }
        animator.SetBool("isGrounded", isGrounded);
    }

    void CheckWallCollision()
    {
        Collider2D left = Physics2D.OverlapBox((Vector2)transform.position + Vector2.left * playerCollider.bounds.extents.x , wallBoxSize, 0, groundMask);
        Collider2D right = Physics2D.OverlapBox((Vector2)transform.position + Vector2.right * playerCollider.bounds.extents.x, wallBoxSize, 0, groundMask);

        if (left || right)
        {
            Collider2D grabbedWall = left == null ? right : left;
            if (!grabbedWall.gameObject.CompareTag("NonWallGrabbable"))
            {
                if (!onWall)
                {
                    lastPosOnWall = transform.position;
                }
                onWall = true;
                wallJumpTimer = Time.time + wallJumpDelay;
                onRightWall = right ? true : false;
                LerpCameraOffset(CameraPosition.Normal);

                if (Mathf.Abs(lastPosOnWall.y - transform.position.y) >= playerCollider.bounds.extents.y)
                {
                    lastPosOnWall = transform.position;
                    int direction = onRightWall ? -1 : 1;
                    Instantiate(dust, new Vector2(transform.position.x - direction * playerCollider.bounds.extents.x,
                        transform.position.y - 0.3f), Quaternion.Euler(0, 0, 90)).transform.localScale *= 0.5f;
                }
            }

        }
        else
        {
            onWall = false;
        }
    }


    void ModifyPhysics()
    {
        bool changingDirections = dir.x * rb.velocity.x < 0;

        if (isGrounded)
        {
            if (onMovingPlatform)
            {
                rb.drag = 0;

            }
            else
            {
                if (Mathf.Abs(dir.x) == 0 && Mathf.Abs(rb.velocity.y) < 0.4f)
                {
                    rb.drag = slowdownSpeed;
                }
                else
                {
                    rb.drag = 0;
                }
            }


        }
        else if (onWall)
        {
            rb.gravityScale = 1;
            if (rb.velocity.y > 0)
            {
                rb.velocity += Vector2.down * slowdownOnWallCollision * Time.fixedDeltaTime;
            }
            else
            {
                rb.velocity += Vector2.down * wallSlideSpeed * Time.fixedDeltaTime;
                if (rb.velocity.y < -maxWallSlideSpeed)
                {
                    rb.velocity = Vector2.down * maxWallSlideSpeed;
                }
            }
        }
        else
        {
            rb.gravityScale = 1;
            rb.drag = slowdownSpeed * 0.15f;
            if (rb.velocity.y < 0)
            {
                rb.gravityScale = fallMultiplier;
                if (rb.velocity.y < currentMaxFallSpeed)
                {
                    rb.velocity = new Vector2(rb.velocity.x, currentMaxFallSpeed);
                    LerpCameraOffset(CameraPosition.Fall);
                }
            }
            else if (rb.velocity.y > 0 && !Input.GetButton("Jump"))
            {
                LerpCameraOffset(CameraPosition.Normal);
                rb.gravityScale = lowJumpMultiplier;
            }
       
            animator.SetFloat("ySpeed", rb.velocity.y);
        }

    }

    void Flip()
    {
        FacingRight = !FacingRight;
        transform.rotation = Quaternion.Euler(0, FacingRight ? 0 : 180, 0);
    }

    public void Jump(float jumpMultiplier, bool spawnDust = true, bool playSound = true)
    {
        if (playSound)
        {
            PlayAudioClip(jumpClip);
        }

        if (spawnDust)
        {
            Instantiate(dust, new Vector2(transform.position.x, playerCollider.bounds.min.y), Quaternion.identity);
        }

        rb.velocity = new Vector2(rb.velocity.x, 0);
        rb.AddForce(Vector2.up * jumpMultiplier, ForceMode2D.Impulse);

        jumpTimer = 0;
        ungroundTimer = 0;
        StartCoroutine(JumpSqueeze(jumpXSqueeze, jumpYSqueeze, jumpSqueezeDuration));

    }

    void WallJump()
    {
        onWall = false;
        wallJumpTimer = 0;
        rb.velocity = Vector2.zero;
        Jump(wallJumpHeight, false);

        int direction = onRightWall ? -1 : 1;
        Instantiate(dust, new Vector2(transform.position.x - direction * playerCollider.bounds.extents.x, transform.position.y - 0.3f), Quaternion.Euler(0, 0, 90));

        rb.AddForce(Vector2.right * accelerationRate * 0.5f * direction, ForceMode2D.Impulse);
        StartCoroutine(DisableMovement(regainControlDelay));

        Flip();

    }

    IEnumerator JumpSqueeze(float xSqueeze, float ySqueeze, float seconds)
    {
        Vector3 originalSize = Vector3.one;

        Vector3 newSize = new Vector3(xSqueeze, ySqueeze, originalSize.z);
        float t = 0;
        while (t <= 1)
        {
            t += Time.deltaTime / seconds;
            characterHolder.transform.localScale = Vector3.Lerp(originalSize, newSize, t);
            yield return null;
        }
        t = 0;
        while (t <= 1)
        {
            t += Time.deltaTime / seconds;
            characterHolder.transform.localScale = Vector3.Lerp(newSize, originalSize, t);
            yield return null;
        }

    }

    void LerpCameraOffset(CameraPosition newPosition)
    {
        if (cameraPosition == newPosition)
            return;
        else
            cameraPosition = newPosition;

        Vector2 newOffset = Vector2.zero;
        switch (newPosition)
        {
            case CameraPosition.Normal:
                newOffset = Vector2.zero;
                break;
            case CameraPosition.Left:
                newOffset = new Vector2(-cameraRunOffset, 0);
                break;
            case CameraPosition.Right:
                newOffset = new Vector2(cameraRunOffset, 0);
                break;
            case CameraPosition.Fall:
                newOffset = maxFallCameraOffset;
                break;
        }
        if (activeLerp != null)
            StopCoroutine(activeLerp);
        activeLerp = StartCoroutine(LerpCameraOffsetCo(newOffset));

    }

    IEnumerator LerpCameraOffsetCo(Vector2 newOffset)
    {
        Vector2 oldOffset = cameraOffset.value;
        
        float t = 0;
        while (t <= 1)
        {
            t += Time.deltaTime / offsetLerpDuration;
            cameraOffset.value = Vector3.Lerp(oldOffset, newOffset, t);
            yield return null;
        }

    }

    void PlayAudioClip(AudioClip clip)
    {
        audioSource.PlayOneShot(clip);
        
    }

    void KillPlayer(Vector2 contact)
    {
        
        playerDeathEvent.Raise();
        PlayAudioClip(deathClip);
        animator.SetBool("playerDeath", true);
        sprite.color = Color.red;
        

        float awayFromEnemyDir = transform.position.x > contact.x ? 1 : -1;

        rb.velocity = (Vector2.right * awayFromEnemyDir + Vector2.up) * deathMoveMultiplier;

        playerCollider.enabled = false;
        enabled = false;
        rb.isKinematic = true;
        rb.gravityScale = 0;

    }

    public void Disable()
    {
        canMove = false;
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Enemy"))
        {
            //playerDeathCo = StartCoroutine(KillPlayer(collision.GetContact(0).point));
        }
        else if (collision.transform.CompareTag("Unkillable"))
        {
           KillPlayer(collision.GetContact(0).point);
        }
        if (collision.transform.CompareTag("MovingPlatform"))
        {
            onMovingPlatform = true;
            currentPlatform = collision.gameObject.GetComponent<MovingPlatform>();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Enemy"))
        {
            float underPlayerY = playerCollider.bounds.min.y;
            float upperEnemyY = collision.bounds.min.y; //collision.ClosestPoint(playerCollider.bounds.min).y;
            if (underPlayerY > upperEnemyY)
            {
                if (playerDeathCo != null)
                {
                    StopCoroutine(playerDeathCo);
                }

                collision.gameObject.GetComponent<IKillable>().Knockout();
                Jump(jumpMultiplier, playSound: false);
                PlayAudioClip(enemyStomp);
            }
            else
            {
                KillPlayer(collision.bounds.center);

            }

        }

  
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("MovingPlatform"))
        {
            onMovingPlatform = false;
        }
    }

    private void OnDrawGizmos()
    {
        Vector2 boxCenter = (Vector2)transform.position + Vector2.down * jumpBoxHeight;
        Gizmos.color = Color.green;
        Gizmos.DrawCube(boxCenter, new Vector2(jumpBoxWidth, groundSkin));

        Gizmos.DrawCube(transform.position + Vector3.right * GetComponent<Collider2D>().bounds.extents.x, wallBoxSize);
        Gizmos.DrawCube(transform.position + Vector3.left * GetComponent<Collider2D>().bounds.extents.x, wallBoxSize);

        Gizmos.color = Color.black;

    }

}


