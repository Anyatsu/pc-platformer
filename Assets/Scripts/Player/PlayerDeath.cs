﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerDeath : MonoBehaviour
{
    [SerializeField] IntVariable deathCount;
    [SerializeField] GameEvent sceneResetEvent;
    private AsyncOperation op;

   void ReloadScene()
    {
        deathCount.value++;
        sceneResetEvent.Raise();
        op = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);
        op.allowSceneActivation = false;
    }

    public void EnterReloadedScene()
    {
        op.allowSceneActivation = true;

    }
}
