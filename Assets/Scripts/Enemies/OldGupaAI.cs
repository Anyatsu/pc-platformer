﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OldGupaAI : MonoBehaviour
{
    private Animator animator;
    private Transform player;
    [SerializeField] private float attackDistance;
    [SerializeField] private float moveDistanceAfterAtk;
    private Vector2 leftEdgePosition;
    private Vector2 rightEdgePosition;

    private Rigidbody2D rb;
    [SerializeField] private float moveSpeed;
    private bool moving;
    private bool attacking;
    private bool nearEdge;

    private bool facingRight;
    private bool playerLeftPlatform;
    private bool playerWentRight;

    [SerializeField] private float groundSkin;
    [SerializeField] private float groundRayOffset;
    [SerializeField] private LayerMask groundLayer;

    [SerializeField] private float minWaitTime, maxWaitTime;
    private float currentTime;





    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        player = FindObjectOfType<PlayerController>().transform;
        facingRight = true;
    }

    private void Start()
    {
        rightEdgePosition = GetEdgePosition(transform.position, 0.5f);
        leftEdgePosition = GetEdgePosition(transform.position, -0.5f);
    }

    void Update()
    {
        float distanceToPlayer = Vector2.Distance(transform.position, player.position);
        bool playerBeyondEdge = player.position.x - rightEdgePosition.x >= 0 || player.position.x - leftEdgePosition.x <= 0;

        if (distanceToPlayer < attackDistance && !playerBeyondEdge && !nearEdge)
        {
            attacking = true;
        }
        else
        {
            if (attacking)
            {
                attacking = false;
                moving = false;
                currentTime = Random.Range(minWaitTime, maxWaitTime);
            }

            else if (currentTime < 0)
            {
                currentTime = Random.Range(minWaitTime, maxWaitTime);

                //Randomly flipping gupa before it starts walking
                moving = !moving;
                if (moving && Random.Range(0, 2) == 1)
                {
                    Flip();
                }
            }
            currentTime -= Time.deltaTime;
        }

        animator.SetBool("attacking", attacking);
        animator.SetBool("moving", moving);
    }

    private void FixedUpdate()
    {
        bool edgeToRight = !Physics2D.Raycast((Vector2)transform.position + Vector2.right * groundRayOffset, Vector2.down, groundSkin, groundLayer);
        bool edgeToLeft = !Physics2D.Raycast((Vector2)transform.position + Vector2.left * groundRayOffset, Vector2.down, groundSkin, groundLayer);
        nearEdge = edgeToLeft || edgeToRight;
        if ((edgeToRight && facingRight) || (edgeToLeft && !facingRight))
        {
            if (!attacking)
                Flip();
        }

        if (attacking)
        {
            Vector2 distance = player.position - transform.position;
            int direction = distance.normalized.x >= 0 ? 1 : -1;

            if ((direction == 1 && !facingRight) || (direction == -1 && facingRight))
            {
                Flip();
            }

            rb.velocity = new Vector2(direction * moveSpeed, rb.velocity.y);
        }
        else
        {
            if (moving)
            {
                rb.velocity = new Vector2(transform.right.x * moveSpeed, rb.velocity.y);
            }
            else
            {
                rb.velocity = Vector2.zero;
            }
        }


    }

    Vector2 GetEdgePosition(Vector2 startingPosition, float increments)
    {

        RaycastHit2D ray;
        float offset = 0;
        Vector2 currentPosition;
        Vector2 edgePosition = Vector2.zero;

        while (edgePosition == Vector2.zero)
        {
            offset += increments;
            currentPosition = (Vector2)transform.position + Vector2.right * offset;
            ray = Physics2D.Raycast(currentPosition, Vector2.down, groundSkin, groundLayer);
            if (ray.collider == null)
            {
                currentPosition.x += increments; //Making the position a bit beyond the edge to allow for better detection
                edgePosition = currentPosition;

            }
        }

        return edgePosition;
    }



    void Flip()
    {
        facingRight = !facingRight;
        transform.rotation = Quaternion.Euler(0, facingRight ? 0 : 180, 0);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Flip();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        Vector2 start = (Vector2)transform.position + Vector2.right * groundRayOffset;
        Gizmos.DrawLine(start, start + Vector2.down * groundSkin);
        start = (Vector2)transform.position + Vector2.left * groundRayOffset;
        Gizmos.DrawLine(start, start + Vector2.down * groundSkin);

    }
}
