﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallerAi : MonoBehaviour
{
    private Animator animator;
    private Transform player;
    [SerializeField] private float attackDistance;

    [SerializeField] private float moveSpeed;
    private bool moving;
    private bool attacking;
    private Rigidbody2D rb;

    [SerializeField] private float minWaitTime, maxWaitTime;
    private float currentTime;

    [SerializeField] private float groundSkin;
    [SerializeField] private LayerMask groundLayer;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    private void Start()
    {
        Vector3 currRot = transform.eulerAngles;
        currRot.y += 180;
        player = FindObjectOfType<PlayerController>().transform;
    }

    void Update()
    {
        if (Vector2.Distance(transform.position, player.position) < attackDistance)
        {
            attacking = true;
            transform.LookAt(player);
            transform.rotation = Quaternion.Euler(0, transform.eulerAngles.y <= 180 ? 0 : 180, 0);
        }
        else
        {
            if (attacking)
            {
                attacking = false;
                moving = false;
                currentTime = Random.Range(minWaitTime, maxWaitTime);

            }

            if (currentTime < 0)
            {
                currentTime = Random.Range(minWaitTime, maxWaitTime);
                moving = !moving;
                if (Random.Range(0, 2) == 1)
                {
                    Flip();
                }
            }

            currentTime -= Time.deltaTime;
        }

        //animator.SetBool("attacking", attacking);
        //animator.SetBool("moving", moving);
    }

    private void FixedUpdate()
    {
        if (attacking)
        {
            Vector2 direction = player.position - transform.position;
            float normalizedX = direction.normalized.x >= 0 ? 1 : -1;

            rb.velocity = new Vector2(normalizedX * moveSpeed, rb.velocity.y);
        }
        else
        {
            if (moving)
            {
                rb.velocity = new Vector2(transform.right.x * moveSpeed, rb.velocity.y);
            }
            else
            {
                rb.velocity = Vector2.zero;
            }
        }

        if (!Physics2D.Raycast(transform.position + transform.right, Vector2.down, groundSkin, groundLayer))
        {
            Flip();
        }
    }

    void Flip()
    {
        Vector3 currRot = transform.eulerAngles;
        currRot.y += 180;
        transform.eulerAngles = currRot;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Flip();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position + transform.right, (Vector2)(transform.position + transform.right) + Vector2.down * groundSkin);
    }
}
