﻿using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;


public class Unkillable_AbmoogAI : MonoBehaviour
{
    [SerializeField] Transform spawnPoint;
    public LayerMask enemyMask;
    public float speed;
    Rigidbody2D myBody;
    Transform myTrans;
    float myWidth;
    public LayerMask playerMask;
    public int radius = 0;
    private Transform player;
    void Start()
    {
        player = GameObject.FindWithTag("Player").transform;
        myTrans = this.transform;
        myBody = this.GetComponent<Rigidbody2D>();
        myWidth = this.GetComponent<BoxCollider2D>().size.x;
    }

    void FixedUpdate()
    {

        //Check to see if there's ground infront of Abmoog.
        Vector2 lineCastPos = myTrans.position - myTrans.right * myWidth;
        Debug.DrawLine(lineCastPos, lineCastPos + Vector2.down);
        bool isGrounded = Physics2D.Linecast(lineCastPos, lineCastPos + Vector2.down, enemyMask);
        Debug.DrawLine(lineCastPos, lineCastPos - myTrans.right.toVector2() * 0.2f);


        //Rotate if there's no ground
        if (!isGrounded) // || isBlocked
        {
            Vector3 currRot = myTrans.eulerAngles;
            currRot.y += 180;
            myTrans.eulerAngles = currRot;
        }

        // Always move forward.
        Vector2 myVel = myBody.velocity;
        myVel.x = -myTrans.right.x * speed;
        myBody.velocity = myVel;
        myBody.angularVelocity = 0;

    }



}


