﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ApoocAI : MonoBehaviour, IKillable
{
    public float maxDistanceFromStart;
    public Vector3 startingPosition;
    public float speed;
    [SerializeField] Transform spawnPoint;
    Rigidbody2D rb;
    private Animator animator;
    [SerializeField] private bool waitingForRespawn;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        animator.SetBool("active", true);
    }

    private void Start()
    {
        startingPosition = new Vector3(transform.position.x, transform.position.y);

    }

    void FixedUpdate()
    {
        transform.position = startingPosition + (Vector3.right * Mathf.Sin(Time.timeSinceLevelLoad * speed) * maxDistanceFromStart);

    }

    void ResetPosition()
    {
        Vector2 pos = transform.position;
        pos.x = startingPosition.x;
        transform.position = pos;
    }

    public void Knockout()
    {
        animator.SetTrigger("isDead");
        animator.SetBool("active", false);
        StartCoroutine(WaitForRespawn());
    }

    IEnumerator WaitForRespawn()
    {
        yield return new WaitUntil(() => waitingForRespawn);
        Vector2 currentPosition = Vector2.positiveInfinity;
        while (Mathf.Abs(currentPosition.x - transform.position.x) > 0.5f)
        {
            currentPosition = startingPosition + (Vector3.right * Mathf.Sin(Time.timeSinceLevelLoad * speed) * maxDistanceFromStart);
            yield return null;
        }
        animator.SetBool("active", true);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {

            //CheckPointManager.instance.KillPlayer();
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        if (!Application.isPlaying)
            startingPosition = transform.position;
        Gizmos.DrawLine(startingPosition + (Vector3.right * -1 * maxDistanceFromStart), startingPosition + (Vector3.right * 1 * maxDistanceFromStart));
    }

}