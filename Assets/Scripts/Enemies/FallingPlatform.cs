﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingPlatform : MonoBehaviour
{ 
    private Animator animator;
    [SerializeField] private LayerMask playerLayer;
    [SerializeField] private float width, height, len;

    private float triggerDelay = 1.5f;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        Vector2 boxCenter = (Vector2)transform.position + Vector2.up * height;
        if (Physics2D.OverlapBox(boxCenter, new Vector2(width, len), 0, playerLayer))
        {
            animator.SetTrigger("Collision");
            enabled = false;
            
            StartCoroutine(DelayedEnable());
        }
    }

    IEnumerator DelayedEnable()
    {
        float time = 0;
        while (time < triggerDelay)
        {
            time += Time.deltaTime;
            yield return null;
        }
        enabled = true;
    }


    private void OnDrawGizmos()
    {
        Vector2 boxCenter = (Vector2)transform.position + Vector2.up * height;
        Gizmos.color = Color.green;
        Gizmos.DrawCube(boxCenter, new Vector2(width, len));
    }
}
