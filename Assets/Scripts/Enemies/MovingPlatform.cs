﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public float maxDistanceFromStart;
    private Vector3 startingPosition;
    [SerializeField] private float acceleration;
    
    private Rigidbody2D rb;
    public float CurrentVelocity
    {
        get { return rb.velocity.x; }
    }

    private void Awake()
    {
        startingPosition = transform.position;
        rb = GetComponent<Rigidbody2D>();
    }

    float timer;
    void FixedUpdate()
    {
        if (Mathf.Abs(rb.velocity.x) > Mathf.Sqrt(Mathf.Abs(acceleration) * maxDistanceFromStart))
        {
            acceleration = -acceleration;
        }
        rb.velocity += (Vector2)transform.right * acceleration * Time.deltaTime;

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        if (!Application.isPlaying)
            startingPosition = transform.position;
        Gizmos.DrawLine(startingPosition, startingPosition + (transform.right * maxDistanceFromStart));
    }
}
