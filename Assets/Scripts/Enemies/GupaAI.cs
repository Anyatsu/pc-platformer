﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GupaAI : MonoBehaviour, IKillable
{
    private Animator animator;
    private PlayerController player;

    private Rigidbody2D rb;
    [SerializeField] private float moveSpeed;
    [SerializeField] private float runSpeed;
    private bool moving;
    private bool running;
    private bool nearEdge;

    private bool facingRight;

    [SerializeField] private float groundSkin;
    [SerializeField] private float groundRayOffset;
    [SerializeField] private LayerMask groundLayer;

    [SerializeField] private float minWaitTime, maxWaitTime;
    private float currentTime;

    [SerializeField] private float distanceOnHit = 5;
    [SerializeField] private float maxDistanceFromStart = 5;
    private Vector2 startPosition;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        player = FindObjectOfType<PlayerController>();
        facingRight = true;
        startPosition = transform.position;
    }
    
    void Update()
    {
        if (currentTime < 0)
        {
            currentTime = Random.Range(minWaitTime, maxWaitTime);

            //Randomly flipping gupa before it starts walking
            if (maxDistanceFromStart != 0)
                moving = !moving;
            if (moving && Random.Range(0, 2) == 1)
            {
                Flip();
            }
            if (maxDistanceFromStart != 0)
                animator.SetBool("moving", moving);
        }
        currentTime -= Time.deltaTime;
        

        
    }

    private void FixedUpdate()
    {
        bool edgeToRight = !Physics2D.Raycast((Vector2)transform.position + Vector2.right * groundRayOffset, Vector2.down, groundSkin, groundLayer);
        bool edgeToLeft = !Physics2D.Raycast((Vector2)transform.position + Vector2.left * groundRayOffset, Vector2.down, groundSkin, groundLayer);
        bool wallInFront = Physics2D.Raycast((Vector2)transform.position, transform.right, groundSkin, groundLayer);
        nearEdge = edgeToLeft || edgeToRight;

        if ((edgeToRight && facingRight) || (edgeToLeft && !facingRight) || wallInFront)
        {
            Flip();
        }

        if (running)
        {
            rb.velocity = new Vector2(transform.right.x * runSpeed, rb.velocity.y);
        }
        else if (moving)
        {
            rb.velocity = new Vector2(transform.right.x * moveSpeed, rb.velocity.y);
            float distanceFromStart = transform.position.x - startPosition.x;
            if ((distanceFromStart < -maxDistanceFromStart && !facingRight) || (distanceFromStart > maxDistanceFromStart && facingRight))
                Flip();
        }
        else
        {
            rb.velocity = Vector2.zero;
        }
    }

    

    void Flip()
    {
        facingRight = !facingRight;
        transform.rotation = Quaternion.Euler(0, facingRight ? 0 : 180, 0);
    }

    public void Knockout()
    {
        if (facingRight != player.FacingRight)
            Flip();
        StopAllCoroutines();
        animator.Rebind();
        StartCoroutine(Run());

    }

    IEnumerator Run()
    {
        moving = false;
        running = true;

        animator.SetBool("running", true);

        float distanceTraveled = 0;
        Vector2 prevPosition = transform.position;
        while(distanceTraveled < distanceOnHit)
        {
            distanceTraveled += Mathf.Abs(transform.position.x - prevPosition.x);
            prevPosition = transform.position;
            yield return null;

        }

        running = false;
        startPosition = transform.position;

        animator.SetBool("running", false);
        animator.SetBool("moving", false);
        currentTime = maxWaitTime;
        moving = false;
        rb.velocity = Vector2.zero;

    }


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + transform.right * distanceOnHit);

        Gizmos.color = Color.green;
        Vector3 start = (Vector2)transform.position + Vector2.right * groundRayOffset;
        Gizmos.DrawLine(start, start + Vector3.down * groundSkin);

        start = (Vector2)transform.position + Vector2.left * groundRayOffset;
        Gizmos.DrawLine(start, start + Vector3.down * groundSkin);

        start = transform.position + Vector3.up * 0.25f;
        Gizmos.DrawLine(start, start + transform.right * groundSkin);

        

        if (!Application.isPlaying)
            startPosition = transform.position;

        Gizmos.color = Color.blue;
        Gizmos.DrawLine(startPosition + Vector2.up * 0.5f - Vector2.left * maxDistanceFromStart,
            startPosition + Vector2.up * 0.5f - Vector2.right * maxDistanceFromStart);



    }
}
