﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour, IKillable
{
    public float time;

    private Animator animator;
    private Rigidbody2D rb;

    void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void Knockout()
    {
        animator.SetTrigger("isDead");
    }
}
