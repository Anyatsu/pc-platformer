﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MoorshAI : MonoBehaviour
{

    public float maxDistanceFromStart;
    public Vector3 startingPosition;
    public float speed;


    private void Start()
    {
        startingPosition = new Vector3(transform.position.x, transform.position.y);

    }
    void Update()
    {
        transform.position = startingPosition + (Vector3.up * Mathf.Sin(Time.time * speed) * maxDistanceFromStart);
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        if (!Application.isPlaying)
            startingPosition = transform.position;
        Gizmos.DrawLine(startingPosition + (Vector3.up * -1 * maxDistanceFromStart), startingPosition + (Vector3.up * 1 * maxDistanceFromStart));
    }

}