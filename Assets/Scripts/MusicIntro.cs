﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicIntro : MonoBehaviour
{
    AudioSource source;
    [SerializeField] AudioSource musicPlayer;

    IEnumerator Start()
    {
        source = GetComponent<AudioSource>();
        source.Play();
        source.loop = false;
        yield return new WaitUntil(() => source.isPlaying == false);

        musicPlayer.Play();
        musicPlayer.loop = true;
        
    }

}
