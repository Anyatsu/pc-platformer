﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
    [SerializeField] AudioMixer mixer;

    [SerializeField] FloatVariable soundsVolume;
    [SerializeField] FloatVariable musicVolume;
    [SerializeField] Slider soundSlider;
    [SerializeField] Slider musicSlider;


    private void Awake()
    {
        mixer.SetFloat("SoundsVolume", soundsVolume.value);
        mixer.SetFloat("MusicVolume", musicVolume.value);

        musicSlider.value = Mathf.Pow(10, musicVolume.value / 20);

        soundSlider.value = Mathf.Pow(10, soundsVolume.value / 20);
    }


    public void SetMusicVolume(float volume)
    {
        musicVolume.value = Mathf.Log10(volume) * 20;
        mixer.SetFloat("MusicVolume", musicVolume.value);
    }

    public void SetSoundsVolume(float volume)
    {
        soundsVolume.value = Mathf.Log10(volume) * 20;
        mixer.SetFloat("SoundsVolume", soundsVolume.value);
    }
}
