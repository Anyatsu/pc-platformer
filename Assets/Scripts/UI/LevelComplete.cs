﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;


public class LevelComplete : MonoBehaviour
{
    [SerializeField] GameObject levelCompleteScreen;
    [SerializeField] float durationUntillFull;
    [SerializeField] float timeBetweenFields;
    PlayerController player;

    [SerializeField] IntVariable[] stats;
    [SerializeField] IntVariable[] amountInLevel;


    private void Awake()
    {
        player = FindObjectOfType<PlayerController>();
    }


    public void ShowFinishScreen()
    {
        levelCompleteScreen.SetActive(true);
        player.Disable();
        StartCoroutine(ShowResults());
    }

    public void LoadNextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }


    IEnumerator ShowResults()
    {
        TextMeshProUGUI[] texts = levelCompleteScreen.transform.GetComponentsInChildren<TextMeshProUGUI>(true);

        foreach(TextMeshProUGUI text in texts)
        {
            text.gameObject.SetActive(false);
        }

        yield return new WaitForSeconds(timeBetweenFields);
        for (int i = 0; i < texts.Length; i++)
        {
            texts[i].gameObject.SetActive(true);
            if (i >= stats.Length)
            {
                break;
            }

            //Increase the field value gradually
            int currentValue = 0;
            float currentTime = 0;
            bool skip = false;
            float interval = durationUntillFull / stats[i].value;
            while (currentValue < stats[i].value && !skip)
            {
                currentValue++;
                texts[i].text = currentValue.ToString();

                while (currentTime < interval)
                {
                    if (Input.GetButtonDown("Jump"))
                    {
                        skip = true;
                        break;
                    }
                    currentTime += Time.deltaTime;
                    yield return null;
                }
                currentTime = 0;
            }

            texts[i].text = stats[i].value.ToString();
            if (i < amountInLevel.Length)
            {
                texts[i].text += "/" + amountInLevel[i].value;
            }
                

            //Wait a bit before showing the next field
            yield return null;
            while (currentTime < timeBetweenFields)
            {
                if (Input.GetButtonDown("Jump"))
                    break;
                currentTime += Time.deltaTime;
                yield return null;
            }
        }

    }
}
