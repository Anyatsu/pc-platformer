﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class PauseMenu : MonoBehaviour
{
    public static bool gamePaused;
    public static bool inSubMenu;

    [SerializeField] GameEvent pauseEvent;
    [SerializeField] GameEvent resumeEvent;

    public GameObject pauseMenuUI;
    public GameObject settingsMenuUI;

    [SerializeField] AudioMixerSnapshot paused;
    [SerializeField] AudioMixerSnapshot unpaused;


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (gamePaused)
            {
                if (inSubMenu)
                {
                    GoBack();
                }
                else
                {
                    Resume();
                }
            }
            else
            {
                Pause();
            }
        }
    }

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1;
        gamePaused = false;
        resumeEvent.Raise();

        SetUnpausedAudioFilter();

    }

    void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0;
        gamePaused = true;
        pauseEvent.Raise();

        SetPausedAudioFilter();

    }

    public void SetPausedAudioFilter()
    {
        paused.TransitionTo(0.1f);
    }

    public void SetUnpausedAudioFilter()
    {
        unpaused.TransitionTo(0.1f);
    }

    public void OpenSettings()
    {
        inSubMenu = true;
        pauseMenuUI.SetActive(false);
        settingsMenuUI.SetActive(true);
    }

    public void GoBack()
    {
        foreach(Transform t in transform)
        {
            if (t.gameObject.name == "LevelReload")
            {
                continue;
            }
            t.gameObject.SetActive(false);
            
        }
        pauseMenuUI.SetActive(true);
        inSubMenu = false;
    }
}
