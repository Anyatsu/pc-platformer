﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAnimation : MonoBehaviour
{
    GameObject[] frames;
    [SerializeField] float delayBetweenFrames;
    int activeFrame;
    float timer;
    private void Awake()
    {
        frames = new GameObject[transform.childCount];
        int i = 0;
        foreach (Transform t in transform)
        {
            frames[i] = t.gameObject;
            frames[i].SetActive(false);
            i++;
        }
    }

    private void Update()
    {
        timer += Time.deltaTime;
        frames[activeFrame].SetActive(true);

        if (timer > delayBetweenFrames)
        {
            timer = 0;

            frames[activeFrame].SetActive(false);
            activeFrame++;
            if (activeFrame >= frames.Length)
            {
                activeFrame = 0;
            }
            frames[activeFrame].SetActive(true);
        }


    }
}
