﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlphaController : MonoBehaviour
{

    [SerializeField] float fadeOutSpeed;
    [SerializeField] float delayBeforeFadeOut;
    CanvasGroup group;
    Coroutine coroutine;

    private void Awake()
    {
        group = GetComponent<CanvasGroup>();
    }


    IEnumerator FadeOutCo()
    {
        yield return new WaitForSeconds(delayBeforeFadeOut);
        while (group.alpha > 0)
        {
            group.alpha -= fadeOutSpeed * Time.deltaTime;
            yield return null;
        }
    }

    public void MaxAlphaAndFade()
    {
        group.alpha = 1;
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
        }
        coroutine = StartCoroutine(FadeOutCo());


    }
}
