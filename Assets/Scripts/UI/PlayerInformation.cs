﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerInformation : MonoBehaviour
{
    private Transform player;
    private CanvasGroup group;
    [SerializeField] private float showUIDistance;
    [SerializeField] private Vector2Variable position;
    [SerializeField] private float yOffset, xOffset;

    void Awake()
    {
        group = GetComponent<CanvasGroup>();
        player = FindObjectOfType<PlayerController>().transform;
    }

    private void Update()
    {
        //Fade out ui when player gets far away

        float distance = Mathf.Abs(player.position.x - transform.position.x);
        if (distance < showUIDistance)
        {
            group.alpha = Mathf.Abs(1 - distance / showUIDistance);
        }

    }


    public void SetPosition()
    {
        transform.position = position.value + Vector2.up * yOffset + Vector2.right * xOffset;
    }
}
