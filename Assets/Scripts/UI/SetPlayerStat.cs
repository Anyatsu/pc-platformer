﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SetPlayerStat : MonoBehaviour
{
    [SerializeField] private IntVariable stat;
    private TextMeshProUGUI text;


    private void Awake()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

    private void Update()
    {
        text.text = stat.value.ToString();
    }

}
