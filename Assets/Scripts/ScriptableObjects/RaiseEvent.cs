﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaiseEvent : MonoBehaviour
{
    [SerializeField] private GameEvent eventToRaise;

    public void Raise()
    {
        eventToRaise.Raise();
    }
}
